package com.wordgame.model

data class CharacterPlaceHolder(
    var character: Char? = null,
    var isVisible: Boolean? = null,
    var isNull: Boolean? = null,
    var tag: String? = null,
)
