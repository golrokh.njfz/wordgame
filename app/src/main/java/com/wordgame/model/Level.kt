package com.wordgame.model

import android.os.Parcelable
import kotlinx.android.parcel.Parcelize

@Parcelize
data class Level
    (
    val id: Long?,
    val words: MutableList<String> = mutableListOf(),
) : Parcelable