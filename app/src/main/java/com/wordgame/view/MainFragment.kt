  package com.wordgame.view

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.navigation.Navigation
import androidx.recyclerview.widget.GridLayoutManager
import com.wordgame.R
import com.wordgame.tools.GamePlayUtil
import kotlinx.android.synthetic.main.fragment_main.*

  const val ARG_LEVEL = "level"
  class MainFragment : Fragment() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_main, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

      setLevelAdapter(view)
    }

      private fun setLevelAdapter(view: View) {
          rcv.layoutManager = GridLayoutManager(context, 3 )
          val levelAdapter = LevelAdapter(){ level ->

              var bundle  = Bundle()
              bundle.putParcelable(ARG_LEVEL, level)
              Navigation.findNavController(view).navigate(R.id.action_mainFragment_to_gameFragment,bundle)
          }
          levelAdapter.getLevelList(GamePlayUtil.createLevels())
          rcv.adapter = levelAdapter


      }
    companion object {
        @JvmStatic
        fun newInstance() =
            MainFragment()
    }
}