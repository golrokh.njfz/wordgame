package com.wordgame.view

import android.R.attr
import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.recyclerview.widget.GridLayoutManager
import androidx.recyclerview.widget.LinearLayoutManager
import com.wordgame.R
import com.wordgame.model.CharacterPlaceHolder
import com.wordgame.model.Level
import com.wordgame.tools.GamePlayUtil
import kotlinx.android.synthetic.main.fragment_game.*



class GameFragment : Fragment() {
    private var level: Level? = null
    private var guessCharacterAdapter: CharacterAdapter? = null
    private var wordsAdapter:CharacterAdapter ? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        arguments?.let {
            level = it.getParcelable(ARG_LEVEL)
        }
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_game, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)




        setGuessAdapter()
        setWordsAdapter(finMaxWordLength())

        btn_game_cancel.setOnClickListener {
            frame_game_guessActionsContainer.visibility = View.VISIBLE
            guessCharacterAdapter?.clear()
        }

        btn_game_acceptAction.setOnClickListener {
            val getWord  = guessCharacterAdapter?.getWord()
            for (i in level!!.words) {
                if (getWord.equals(i)) {
                    Toast.makeText(context, "کلمه درست حدس زده شد: $getWord", Toast.LENGTH_SHORT)
                        .show()
                    btn_game_cancel.performClick()
                    getWord?.let { it1 -> wordsAdapter?.makeWordVisible(it1) }
                    return@setOnClickListener
                }else{
                    btn_game_cancel.performClick()
                    Toast.makeText(context, "صحیح نیست", Toast.LENGTH_SHORT).show()

                }
            }
        }
    }

   private fun setWordsAdapter(maxLength:Int){

        wordsAdapter = CharacterAdapter {

        }
        rv_game_words.layoutManager = GridLayoutManager(context,maxLength,GridLayoutManager.VERTICAL, false)
        rv_game_words.adapter = wordsAdapter
        val wordsCharactersPlaceHolders: MutableList<CharacterPlaceHolder> = ArrayList()
        for (i in level!!.words.indices) {
            for (j in 0 until maxLength) {
                val characterPlaceHolder = CharacterPlaceHolder()
                if (j < level!!.words[i].length) {
                    characterPlaceHolder.character = level!!.words[i][j]
                    characterPlaceHolder.isNull = false
                    characterPlaceHolder.isVisible = false
                    characterPlaceHolder.tag= (level!!.words[i])
                } else {
                    characterPlaceHolder.isNull = true
                }
                wordsCharactersPlaceHolders.add(characterPlaceHolder)
            }
        }
        wordsAdapter?.getCharacters(wordsCharactersPlaceHolders)

    }
    private fun setGuessAdapter(){
        val uniqueCharacters  = level?.words?.let { GamePlayUtil.extractUniqueCharacters(it) }
        val characterPlaceHolders: MutableList<CharacterPlaceHolder> = ArrayList()
        for (i in uniqueCharacters!!.indices) {
            val characterPlaceHolder = CharacterPlaceHolder()
            characterPlaceHolder.isVisible = true
            characterPlaceHolder.character = uniqueCharacters[i]
            characterPlaceHolders.add(characterPlaceHolder)
        }
        rv_game_characters.layoutManager =  LinearLayoutManager(context, LinearLayoutManager.HORIZONTAL, false)
        val characterAdapter = CharacterAdapter(){
            frame_game_guessActionsContainer.visibility = View.VISIBLE
            guessCharacterAdapter?.add(it)
        }
        characterAdapter.getCharacters(characterPlaceHolders)
        rv_game_characters.adapter = characterAdapter

        guessCharacterAdapter = CharacterAdapter {

        }
        rv_game_guess.adapter = guessCharacterAdapter
        rv_game_guess.layoutManager =  LinearLayoutManager(context, LinearLayoutManager.HORIZONTAL, false)
    }

    private fun finMaxWordLength():Int{
        var maxWordLength = 0
        for (word in level?.words!!){
            if (word.length > maxWordLength){
                maxWordLength = word.length
            }

        }
        return maxWordLength
    }


    companion object {
        @JvmStatic
        fun newInstance(level: Level) =
            GameFragment().apply {
                arguments = Bundle().apply {
                    putParcelable(ARG_LEVEL, level)
                }
            }
    }
}