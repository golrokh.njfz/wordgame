package com.wordgame.view

import android.media.browse.MediaBrowser
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.wordgame.R
import com.wordgame.model.Level
import kotlinx.android.synthetic.main.item_level.view.*

class LevelAdapter(
    private var itemCallback: ((level: Level) -> Unit )
): RecyclerView.Adapter<LevelAdapter.LevelViewHolder>() {

    private var levelList = mutableListOf<Level>()


    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): LevelViewHolder {
        val view =
            LayoutInflater.from(parent.context).inflate(R.layout.item_level, parent, false)

        view.setOnClickListener {
            view.tag?.let {  tag ->
                if (tag is Level){
                    itemCallback(tag)
                }
            }
        }
        return LevelViewHolder(view)
    }

    override fun onBindViewHolder(holder: LevelViewHolder, position: Int) {
        holder.bind(levelList[position])
    }

    override fun getItemCount(): Int {
      return levelList.size
    }



    fun getLevelList(levels:MutableList<Level>){
        this.levelList = levels
    }

    inner class LevelViewHolder(private var itemView: View) : RecyclerView.ViewHolder(itemView) {
        fun  bind(level: Level) {
            itemView.tv_level_num.text = level.id.toString()

            itemView.tag = level

        }
    }
}