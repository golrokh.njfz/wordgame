package com.wordgame.view

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.wordgame.R
import com.wordgame.model.CharacterPlaceHolder
import kotlinx.android.synthetic.main.item_char.view.*
import java.lang.StringBuilder

class CharacterAdapter(
   private var itemClicked :(( characterPlaceHolder:CharacterPlaceHolder ) -> Unit)
):RecyclerView.Adapter<CharacterAdapter.CharacterViewHolder>() {


    private var characterPlaceHolders  = mutableListOf<CharacterPlaceHolder>()

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): CharacterViewHolder {
        val view =
            LayoutInflater.from(parent.context).inflate(R.layout.item_char, parent, false)

        view.setOnClickListener {
            view.tag?.let{
                if(it is CharacterPlaceHolder){
                    itemClicked(it)
                }
            }
        }

        return CharacterViewHolder(view)
    }

    override fun onBindViewHolder(holder: CharacterViewHolder, position: Int) {
        holder.bind(characterPlaceHolders[position])
    }

    override fun getItemCount(): Int {
      return  characterPlaceHolders.size
    }


    fun add(characterPlaceHolder: CharacterPlaceHolder){
       this.characterPlaceHolders.add(characterPlaceHolder)
       notifyItemInserted(characterPlaceHolders.size-1)
   }
    fun  clear(){
         this.characterPlaceHolders.clear()
        notifyDataSetChanged()
    }
    fun getWord(): String {
        var stringBuilder = StringBuilder()
        for (char in characterPlaceHolders) {
            stringBuilder.append(char.character)
        }
        return stringBuilder.toString()
    }

    fun makeWordVisible(word:String){
        characterPlaceHolders.forEachIndexed{ index, char ->
            if (char.tag != null && char.tag.equals(word)) {
                char.isVisible = true
                notifyItemChanged(index)
            }
        }
    }


    inner class CharacterViewHolder(private var view:View):
        RecyclerView.ViewHolder(view) {
        fun bind(characterPlaceHolder: CharacterPlaceHolder){

            if (characterPlaceHolder.isVisible == true) {
                view.tv_char.text = characterPlaceHolder.character.toString()
                view.tv_char.visibility = View.VISIBLE
            }else{
                view.tv_char.visibility = View.INVISIBLE
            }
            if (characterPlaceHolder.isNull == true) {
                itemView.background = null
            }else{
                itemView.setBackgroundResource(R.drawable.background_rv_item)
            }
            view.tag = characterPlaceHolder
        }

    }

     fun getCharacters(characterPlaceHolders:MutableList<CharacterPlaceHolder>){
        this.characterPlaceHolders = characterPlaceHolders
    }

}