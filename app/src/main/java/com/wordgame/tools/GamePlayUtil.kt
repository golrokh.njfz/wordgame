package com.wordgame.tools

import  com.wordgame.model.Level

object GamePlayUtil {
    fun createLevels(): MutableList<Level> {
        val levels: MutableList<Level> = mutableListOf<Level>()
        val level1 = Level(1)
        level1.words.add("اش")
        level1.words.add("اتش")
        level1.words.add("مشت")
        levels.add(level1)
        val level2 = Level(2)
        level2.words.add("خاک")
        level2.words.add("کاخ")
        levels.add(level2)
        val level3 = Level(3)
        level3.words.add("اهو")
        level3.words.add("هوا")
        levels.add(level3)
        val level4 = Level(4)
        level4.words.add("شیر")
        level4.words.add("ریش")
        level4.words.add("ری")
        level4.words.add("شر")
        levels.add(level4)
        val level5 = Level(5)
        level5.words.add("فک")
        level5.words.add("کف")
        level5.words.add("کفش")
        level5.words.add("کشف")
        level5.words.add("شک")
        levels.add(level5)
        val level6 = Level(6)
        level6.words.add("خرس")
        level6.words.add("سرخ")
        level6.words.add("سر")
        level6.words.add("خر")
        level6.words.add("رس")
        levels.add(level6)
        return levels
    }

    fun extractUniqueCharacters(words: MutableList<String>): MutableList<Char> {
        val characters: MutableList<Char> = ArrayList()
        for (i in words.indices) {
            for (j in words[i].indices) {
                if (!characters.contains(words[i][j])) {
                    characters.add(words[i][j])
                }
            }
        }
        return characters
    }
}